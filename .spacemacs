;;; package --- spacemacs config
;; -*- mode: dotspacemacs -*-
;;; Commentary:
;;; Code:
(add-to-list 'exec-path              "~/.cabal/bin/")
(add-to-list 'custom-theme-load-path "~/.emacs.d/private/themes")

(defun dotspacemacs/layers ()
  "Configuration Layers declaration."
  (setq-default
   dotspacemacs-configuration-layers '(
     gtags
     semantic
     smex
     osx
     ;; org
     ;; markdown
     ;; ansible
     tmux
     fasd
     (perspectives :variables
              perspective-enable-persp-projectile nil)
     better-defaults
     ;; version-control
     (syntax-checking :variables
              syntax-checking-enable-tooltips t)
     (ibuffer :variables
              ibuffer-group-buffers-by 'projects)
     (colors  :variables
              colors-enable-rainbow-identifiers nil
              colors-enable-rainbow-mode t)
     (auto-completion :variables
              auto-completion-enable-help-tooltip t
              auto-completion-enable-snippets-in-popup t
              auto-completion-enable-sort-by-usage t
              auto-completion-return-key-behavior 'complete
              auto-completion-tab-key-behavior 'cycle
              auto-completion-complete-with-key-sequence "H-SPC")
     (git     :variables
              git-magit-status-fullscreen t)
     ;; (haskell :variables
     ;;          haskell-enable-ghci-ng-support t
     ;;          haskell-enable-shm-support t
     ;;          haskell-enable-hindent-style "andrew-gibiansky")
     (shell   :variables
              shell-default-shell 'shell)
     php
     html
     evil-snipe
     emacs-lisp
     )
   dotspacemacs-additional-packages '(window-purpose
                                      git-gutter
                                      ;; mouse
                                      ;; xt-mouse
                                      ;; mouse+
                                      ;; mouse3
                                      ;; scroll-restore
                                      tabbar
                                      )
   dotspacemacs-excluded-packages   '(php-extras
                                      avoid)
   )
  )

(defun dotspacemacs/init ()
  "Initialization function: called at the very startup of Spacemacs initialization before layers configuration."
  (setq-default
   ;; --- Mouse related stuff -------------------------------
   mouse-wheel-down-event                'mouse-4
   mouse-wheel-up-event                  'mouse-5
   focus-follows-mouse                   t
   mouse-1-click-follows-link            'double
   mouse-1-click-in-non-selected-windows nil
   ;; track-mouse            t
   ;; --- Primary keys --------------------------------------
   dotspacemacs-leader-key                  "SPC"
   dotspacemacs-emacs-leader-key            "H-SPC"
   dotspacemacs-major-mode-leader-key       ","
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   evil-escape-key-sequence                 "fa"
   evil-move-cursor-back                    nil
   ;; --- Other crap ----------------------------------------
   dotspacemacs-startup-lists         '(projects recents)
   dotspacemacs-themes                '(solarized-darcula monokai)
   yas-snippet-dirs                   '("~/.emacs.d/private/snippets")
   magit-repository-directories       '("~/Workspace/")
   paradox-github-token               'b59b36c4cabebdf4f2ba36dafdb2d8b84ca1a521
   dotspacemacs-smooth-scrolling       t
   dotspacemacs-persistent-server      t
   dotspacemacs-delete-orphan-packages t
   desktop-save-mode                   t
   smartparens-strict-mode             nil
   make-backup-files                   nil
   auto-save-default                   nil
   )
  )

(defun dotspacemacs/config ()
  "Configuration function:  This function is called at the very end of Spacemacs initialization after layers configuration."
  (setq-default
   linum-delay                     t
   blink-matching-paren            t
   imenu-use-popup-menu            t
   window-sides-slots             '(1 nil 1 nil)
   window-sides-vertical           t
   purpose-preferred-prompt       'auto
   helm-split-window-default-side 'bottom
   helm-split-window-in-side-p     t
   helm-always-two-windows         nil
   helm-full-frame                 nil
   git-gutter:update-interval      2
   ;; Scrolling stuff
   ;; truncate-lines                  t
   ;; default-truncate-lines          t
   scroll-margin                   0
   scroll-conservatively           10000
   scroll-up-aggressively          0.1
   scroll-down-aggressively        0.1
   scroll-preserve-screen-position t
   mouse-wheel-progressive-speed   nil
   ;; auto-window-vscroll             nil
   ;; view-echo-area-messages         nil
   ;; hlt-use-overlays-flag           nil
   ;; scroll-restore-handle-cursor    t
   ;; scroll-restore-cursor-type      nil
   ;; scroll-restore-jump-back        t
   )

  ;; (require 'mouse)
  ;; (require 'xt-mouse)
  ;; (require 'mouse+)
  ;; (require 'mouse3)
  ;; (xterm-turn-on-modify-other-keys)
  ;; (xterm-mouse-mode t)
  ;; (defun track-mouse (e))
  ;; (define-key minibuffer-inactive-mode-map [down-mouse-1] nil)
  ;; (define-key minibuffer-inactive-mode-map [mouse-1]      nil)
  ;; (global-set-key [down-mouse-1]        'mouse-flash-position)
  ;; (global-set-key [down-mouse-2]        'mouse-flash-position-or-M-x)
  ;; (global-set-key [S-down-mouse-2]      'mouse-scan-lines-or-M-:)
  ;; (global-set-key [mode-line C-mouse-1] 'mouse-tear-off-window)
  ;; (global-set-key [mouse-movement]      'mouse-flash-posn-track)

  (require 'tabbar)
  (tabbar-mode          t)
  ;; (tabbar-mwheel-mode   t)
  ;; (tabbar-local-mode    t)
  (blink-cursor-mode    t)
  (mouse-wheel-mode     t)
  (purpose-mode         t)
  (popwin-mode          t)
  (tooltip-mode         t)
  ;; (neotree)
  ;; (global-flycheck-mode t)
  ;; (global-linum-mode    t)
  ;; (menu-bar-mode        t)
  ;; (global-auto-highlight-symbol-mode t)
  ;; (global-highlight-parentheses-mode t)
  ;; Key bindings here
  (evil-leader/set-key "§"         'kill-buffer-and-window)
  (global-set-key      (kbd "§")   'kill-this-buffer)
  (global-set-key      (kbd "H-a") 'next-buffer)
  (global-set-key      (kbd "H-b") 'previous-buffer)
  (global-set-key      [end]       'end-of-line)
  (global-set-key      [home]      'beginning-of-line)
  ;; (require  'scroll-restore)
  ;; (scroll-restore-mode  t)

  (require 'window-purpose-x)
  (purpose-x-popwin-setup)
  (purpose-x-persp-setup)
  (purpose-x-kill-setup)
  (purpose-x-magit-single-on)

  ;; (purpose-load-window-layout "~/.emacs.d/private/purpose-f.layout")
  (setq projectile-switch-project-action 'neotree-projectile-action)

  ;; (require 'git-gutter)
  ;; (git-gutter:linum-setup)
  ;; ;; (global-git-gutter-mode t)
)

(provide '.spacemacs)
;;; .spacemacs ends here
